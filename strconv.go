package main

import (
	"fmt"
	"strconv"
)

func main() {
	boolean, err := strconv.ParseBool("True")
	
	if err == nil {
		fmt.Println(boolean)
	} else {
		fmt.Println("error:", err.Error())
	}

	number, err := strconv.ParseInt("10000", 10, 32)
	if err == nil {
		fmt.Println(number)
	} else {
		fmt.Println(err.Error())
	}

	valueStr := strconv.FormatInt(1000, 2)
	fmt.Println(valueStr)

	valueInt, _ := strconv.Atoi("456")
	fmt.Println(valueInt)
}