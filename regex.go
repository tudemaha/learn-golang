package main

import (
	"fmt"
	"regexp"
)

func main() {
	regex := regexp.MustCompile("a[a-z]u")

	// check if the string match with the regex
	fmt.Println(regex.MatchString("aku"))
	fmt.Println(regex.MatchString("asq"))

	// find all string that match with the regex (-1 if you want to find all)
	fmt.Println(regex.FindAllString("aku dia mereka dodi ahu ahi", -1))
}