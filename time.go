package main

import (
	"fmt"
	"time"
)

func main() {
	now := time.Now()
	fmt.Println(now.Date())
	fmt.Println(now.Month())
	fmt.Println(now.Year())
	fmt.Println(now.Date())

	utc := time.Date(2023, time.January, 5, 15, 50, 0, 0, time.UTC)
	fmt.Println(utc)

	// layout use RFC 
	parse, err := time.Parse("2006-01-02", "2023-01-05")
	if err == nil {
		fmt.Println(parse)
	} else {
		fmt.Print(err.Error())
	}
}