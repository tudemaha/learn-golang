package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Contains("Tude Maha", "Tude"))
	fmt.Println(strings.Split("Tude Maha", " "))
	fmt.Println(strings.ToLower("Tude Maha"))
	fmt.Println(strings.ToUpper("Tude Maha"))
	fmt.Println(strings.ToTitle("ada apa dengan ikom?"))
	fmt.Println(strings.Trim("     Ada Apa dengan Ilkom??    ", " "))
	fmt.Println(strings.ReplaceAll("TUdee Maha", "TUdee", "Tude"))
}