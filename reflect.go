package main

import (
	"fmt"
	"reflect"
)

type TestingUser struct {
	Name string `required:"true"`
}

func isValid(value interface{}) bool {
	valueType := reflect.TypeOf(value)

	for i := 0; i < valueType.NumField(); i++ {
		field := valueType.Field(i)
		if field.Tag.Get("required") == "true" {
			data := reflect.ValueOf(value).Field(i).Interface()
			if data == "" {
				return false
			}
		}
	}
	return true
}

func main() {
	// show the type of data
	sample := TestingUser{"Mahardika"}
	sampleType := reflect.TypeOf(sample)
	fmt.Println(sampleType)

	// show the field name
	structField := sampleType.Field(0)
	fmt.Println(structField.Name)

	// show the tag of struct
	required := structField.Tag.Get("required")
	fmt.Println(required)

	fmt.Println(isValid(sample))
}