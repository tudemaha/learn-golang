package main

import (
	"fmt"
	"os"
)

func main() {
	// mendapatkan argument
	arguments := os.Args
	fmt.Println(arguments)

	/*
	cara penggunaan: 
	go run os.go arg1 arg2 dll
	*/

	// fmt.Println(arguments[0])
	// fmt.Println(arguments[1])
	// fmt.Println(arguments[2])


	// hostname
	hostname, error := os.Hostname()
	if error == nil {
		fmt.Println(hostname)
	} else {
		fmt.Println(error.Error())
	}

	// get environment variables
	testing := os.Getenv("GOPATH")
	fmt.Println(testing)
}