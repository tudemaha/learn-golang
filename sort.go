package main

import (
	"fmt"
	"sort"
)

// the struct
type user struct {
	name string
	age  int
}

// make alias
type userSlice []user

// make the contract
func (value userSlice) Len() int {
	return len(value)
}

func (value userSlice) Less(i, j int) bool {
	return value[i].age < value[j].age
}

func (value userSlice) Swap(i, j int) {
	value[i], value[j] = value[j], value[i]
}

func main() {
	// make the slice first
	users := userSlice {
		{"Mahardika", 19},
		{"Andi", 20},
		{"Edo", 18},
	}

	// before sort
	fmt.Println(users)
	// sort
	sort.Sort(users)
	// after sort
	fmt.Println(users)
}