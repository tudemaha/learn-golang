package main

import (
	"flag"
	"fmt"
)

// memparsing command line arguments
func main() {
	username := flag.String("username", "root", "Your username")
	password := flag.String("password", "root", "Your password")

	flag.Parse()

	/*
	how to use:
	go run flag.go --username yourusername --password yourpassword
	or
	go run flag.go -username=yourusername -password=yourpassword
	*/
	fmt.Println(*username, *password)
}