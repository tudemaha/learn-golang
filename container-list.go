package main

import (
	"container/list"
	"fmt"
)

func main() {
	// crate double linked list
	data := list.New()

	// PushBack to push data to back
	// PushFront to push data to front
	data.PushBack("Tude")
	data.PushBack(1)
	data.PushBack("Maha")

	// iterate
	for value := data.Front(); value != nil; value = value.Next() {
		fmt.Println(value.Value)
	}
}